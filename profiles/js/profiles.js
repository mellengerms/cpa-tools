$(document).ready(function() {

	if (Modernizr.touch) {
		var Puzzle = new Puzzle_Touch({
			display: new Puzzle_Display(),
		});
	}
	else {

		$(".stage p").hide();
		
		var Puzzle = new Puzzle_Drag({
			display: new Puzzle_Display(),
		});
	}

});

/* 
 * Used by both: show profiles, check
 */
var Puzzle_Display = function(data) {
	this.code = false;
	this.prof = '[data-profile]';
	this.init();
};
Puzzle_Display.prototype = {
	init: function() {},
	check: function() {
		return $('[data-stage]').length == $('[data-stage].active').length;
	},
	set: function() {
		var l = String($('.left [data-piece].active').attr('data-piece'));
		var r = String($('.right [data-piece].active').attr('data-piece'));
		this.code = l != 'undefined' && r != 'undefined' ? l + r : false;	
	},
	display: function(before,after) {
		var Obj = this;
		var before = typeof before === 'undefined' ? function() {} : before;
		var after = typeof after === 'undefined' ? function() {} : after;
		if (Obj.code) {
			var item = $('[data-profile="' + Obj.code + '"]');
			$('.profile.initial').slideUp(Obj.eta);
			console.log('yep');
			$(Obj.prof).not(item).slideUp(Obj.eta);
			before();
			$(item).slideDown(Obj.eta,function() {
				var body = $('html, body');
				body.stop().animate({
					scrollTop: $(item).offset().top,
				},Obj.eta);
				after();
			});
		}
		else {
			before();
			$(Obj.prof).slideUp(Obj.eta,function() {
				after();
			});
		}

	}
};

/* 
 * Used by both: mobile only
 */
var Puzzle_Touch = function(data) {
	this.piece = '[data-piece]';
	this.cont = '[data-cont]';
	this.eta = 300;
	//Define by user
	for (var i in data) {
		this[i] = data[i];
	}

	this.init();
};
Puzzle_Touch.prototype = {
	init: function() {
		var Obj = this;
		$(window).resize(function() {
			$(Obj.piece + '.staged').each(function() {
				Obj.reposition(this);
			});
		});
		$(Obj.piece).click(function() {
			var item = this;
			if ($(item).attr('data-piece') == 'stage') {
				// consoe
				var tmp_side = $(item).parent().attr('data-stage');
				item = $('.' + tmp_side + ' ' + Obj.piece + '.staged');
			}
			$('.staging_cont').removeClass('active_it');
			
			$(item).toggleClass('active');
			var side = $(item).parent().parent().hasClass('left') || $(item).parent().hasClass('left') ? 'left' : 'right';
			var stage = $('[data-stage="' + side + '"]');

			if ($(stage).hasClass('active')) { //Remove active
				Obj.shift($('.' + side + ' ' + Obj.piece + '.active').not(item),side,true);
			}

			if ($(item).hasClass('staged')) { //Remove
				// $(Obj.piece,stage).hide();
				Obj.shift(item,side,true);
			}
			
			if ($(item).hasClass('active')) { //Add
				Obj.shift(item,side);
			}
		});
	},
	shift: function(item,side,ret) {
		var Obj = this;
		var ret = typeof ret === 'undefined' ? false : true;
		var stage = $('[data-stage="' + side + '"]');;
		
		if (ret) { //Remove
			var pos = {
				top: 0,
				left: 0,
			};
			var before = function(it) {
				$(stage).removeClass('staged').parent().removeClass('staged');
				$(it).removeClass('staged').parent().removeClass('staged');
			};
			var after = function(it) {
				var par = $(it).parent();
				$(stage).removeClass('active');
				$(it).removeClass('active').removeAttr('style');
			};
		}
		else { //Place
			var our_pos = $(item).offset();
			var cont_pos = $(stage).offset();
			var pos = {
				top: cont_pos.top - our_pos.top,
				left: cont_pos.left - our_pos.left,
			};
			var before = function(it) {};
			var after = function(it) {
				var par = $(it).parent();
				$(stage).addClass('active');
				$(it).addClass('staged').parent().addClass('staged');
			};
		}
		
		//Enact
		$('.staging_cont').addClass('moving_it');
		$(item).addClass('moving').parent().addClass('moving');
		$(stage).addClass('moving');
		$('[data-stage]').not(stage).addClass('staying');
		before(item);
		$(item).animate(pos,Obj.eta,function() {
			Obj.set_title(side,item);
			$('.staging_cont').removeClass('moving_it');
			$(item).removeClass('moving').parent().removeClass('moving');
			$(stage).removeClass('moving');
			$('[data-stage]').not(stage).removeClass('staying');
			Obj.display.set();
			Obj.display.display(function() { //Before

			},function() { //After
				// Reposition after slide show
				$(Obj.piece + '.staged').each(function() {
					Obj.reposition(this);
				});
			});
			after(item);
			if (Obj.display.check()) {
				$('.staging_cont').addClass('active_it');
			}
		});
	},
	set_title: function(side,item) {
		$('[data-stage="' + side + '"] ' + this.piece + ' span').text($('span',item).text());
	},
	reposition: function(item) {
		var our_pos = $(item).parent().offset();
		var side = $(item).parent().parent().hasClass('left') || $(item).parent().hasClass('left') ? 'left' : 'right';
		var stage = $('[data-stage="' + side + '"]');
		var cont_pos = $(stage).offset();
		$(item).css({
			top: cont_pos.top - our_pos.top,
			left: cont_pos.left - our_pos.left,
		});
	}
};


/* 
 * Used by non touch screens
 */
var Puzzle_Drag = function(data) {
	this.piece = '[data-piece]';
	this.cont = '[data-cont]';
	this.eta = 300;
	this.left = "",
	this.right = ""
	//Define by user
	for (var i in data) {
		this[i] = data[i];
	}

	this.init();
};
Puzzle_Drag.prototype = {
	init: function() {
		var Obj = this;

		$(window).resize(function() {
			$('.m, .f').stop().css({ 
				width: '',
				height: '',
			});
			$('.staged:not(.moving) .m, .staged:not(.moving) .f').each(function() { //Set location for all live
				Obj.reposition($(this),function() {});
			});
		});

		loadScript("../js/vendor/jquery-ui.min.js", function () {
			var drag = {
				revert: 'invalid',
	   			snap: '[data-stage=left]',
	   			snapMode: 'corner',
		        snapTolerance: '20',
		        start: function() {
		        	$(this).parent().removeClass('staged');
		        	$(this).addClass('moving').parent().addClass('moving');
		        },
			};
			drag.stack = '.f';
	   		$('.f').draggable(drag);

			drag.stack = '.m';
	   		$('.m').draggable(drag);

			$('[data-stage=left]').droppable({
				accept: ".f",
				drop: function(e, ui){
					//ease piece to the middle
					Obj.reposition(ui.draggable,function() {
						Obj.left = $(ui.draggable).attr('data-piece');
						$(ui.draggable).parent().addClass('staged');
						$(ui.draggable).removeClass('moving').parent().removeClass('moving');
						//if there is a piece there already pop it back
						var other = $('.staged .f').not(ui.draggable);
						$(other).show().addClass('moving').parent().addClass('moving');
						Obj.reposition(other,function() {
							$(other).css({ zIndex: 1 }).removeClass('moving').parent().removeClass('moving').removeClass('staged').removeAttr('style');
						},true,true);
						// once both are placed, replace with connected one, and load the profile
						Obj.loadProfile();
					},true);
				}
			});

			$('[data-stage=right]').droppable({
				accept: ".m",
				drop: function(e, ui){
					var Drop = this;
					Obj.reposition(ui.draggable,function() {
						Obj.right = $(ui.draggable).attr('data-piece');
						$(ui.draggable).parent().addClass('staged');
						$(ui.draggable).removeClass('moving').parent().removeClass('moving');
						//if there is a piece there already pop it back
						var other = $('.staged .m').not(ui.draggable);
						$(other).show().addClass('moving').parent().addClass('moving');
						Obj.reposition(other,function() {
							$(other).css({ zIndex: 1 }).removeClass('moving').parent().removeClass('moving').removeClass('staged').removeAttr('style');
						},true,true);
						// once both are placed, replace with connected one, and load the profile
						Obj.loadProfile();
					},true);
				}
			});
		});
	},
	reposition: function(item,end,ease,ret) {
		var Obj = this;
		ease = typeof ease === 'undefined' ? false : ease;
		end = typeof end === 'undefined' ? function() {} : end;
		ret = typeof ret === 'undefined' ? false : ret;

		var our_pos = $(item).parent().offset();
		var side = $(item).parent().parent().hasClass('left') || $(item).parent().hasClass('left') ? 'left' : 'right';
		var stage = $('[data-stage="' + side + '"]');
		var cont_pos = $(stage).offset();
		if (ret) {
			var pos = {
				top: 0,
				left: 0,
			};	
		}
		else {
			var pos = {
				top: cont_pos.top - our_pos.top,
				left: cont_pos.left - our_pos.left,
			};
		}
		

		if (!ease) {
			$(item).css(pos);
			end();
		}
		$(item).animate(pos,(ret ? 50 : Obj.eta),function() {
			end();
		});
	},
	loadProfile: function() {
		var Obj = this;

		if(Obj.left && Obj.right){
			$('.staged:not(.moving) .f, .staged:not(.moving) .m').hide();
			$('.staging_cont').addClass('active_it');
			$('[data-stage=left] span').html($('.staged:not(.moving) .f span').html());
			$('[data-stage=right] span').html($('.staged:not(.moving) .m span').html());
			$('[data-stage]').css('visibility','visible').addClass('active');
			var code = Obj.left + Obj.right
			var item = $('[data-profile="' + code + '"]');
			console.log('yep');
			$('.profile.initial').slideUp(Obj.eta);
			$('.profile').slideUp(Obj.eta);
			$(item).slideDown(Obj.eta,function() {
				$('.staged:not(.moving) .m, .staged:not(.moving) .f').each(function() { //
					Obj.reposition($(this),function() {},false);
				});
				var body = $('html, body');
				body.stop().animate({
					scrollTop: $(item).offset().top,
				},Obj.eta);
			});
		}

	}

};



function loadScript(url, callback) {

  var script = document.createElement("script")
  script.type = "text/javascript";

  if (script.readyState) { //IE
      script.onreadystatechange = function () {
          if (script.readyState == "loaded" || script.readyState == "complete") {
              script.onreadystatechange = null;
              callback();
          }
      };
  } else { //Others
      script.onload = function () {
          callback();
      };
  }

  script.src = url;
  document.getElementsByTagName("head")[0].appendChild(script);
}

